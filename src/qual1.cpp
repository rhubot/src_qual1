/**
 * Copyright 2017 Davud Rusbarsky
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Approach
 *   Find color of button using 2D image (/multisense/camera/right/image_color) 
 *   Find pixel in 2D image that corresponds to center of button
 *   Find which 3D point corresponds to that pixel
 *   Report findings
 */

/**
 * Alternate solution A
 *   Map out all the locations of each button on the panel relative to the panel itself and store values in a lookup table
 *   Compare 3D point cloud and laser data to the model of the panel to find location of panel
 *   When a button turns on, identify which button turned on
 *   Look up that button in our lookup table and report the value
 */

#include <iostream>
#include <math.h>

#include <ros/ros.h>
#include <ros/subscribe_options.h>
#include <std_msgs/String.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/JointState.h>

#include <boost/thread.hpp>
#include <boost/algorithm/string.hpp>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <tf/transform_listener.h>
#include <pcl_ros/transforms.h>

#include <visualization_msgs/Marker.h>

#include <srcsim/Console.h>

using namespace cv;
using namespace std;

static const std::string OPENCV_WINDOW    = "Image window";
static const std::string THRESHOLD_WINDOW = "Thresholded Image";

// Note: Hue eventually gets stored in a uchar by OpenCV. Divide the normal hue by 2 to get the desired effect.
static const int RED_HUE           =   0; // h = h/2... 0/2 = 0
static const int RED_SATURATION    = 255;
static const int RED_VALUE         = 255;
static const int BLUE_HUE          = 120; // h = h/2... 240/2 = 120
static const int BLUE_SATURATION   = 255;
static const int BLUE_VALUE        = 255;
static const int GREEN_HUE         = 60; // h = h/2... 120/2 = 60
static const int GREEN_SATURATION  = 255;
static const int GREEN_VALUE       = 255;
static const int HSV_WINDOW        =  20;

static const int CURRENT_COLOR_RED   = 0;
static const int CURRENT_COLOR_BLUE  = 1;
static const int CURRENT_COLOR_GREEN = 2;

static const int NO_BUTTON_FOUND = -1;

class Qual1
{
    // Misc ROS stuff we need
    ros::NodeHandle nh_;
    
    // Variables we'll need for receiving and processing point cloud data
    ros::Subscriber sub;
    tf::TransformListener listener;
    
    pcl::PointXYZRGB buttonLocation;
    
    // Variables we'll need for receiving and processing images from the camera
    image_transport::ImageTransport it_;
    image_transport::Subscriber image_sub_;
    bool shouldShowDefaultImg_;
    cv::Mat defaultImage_;

    cv_bridge::CvImagePtr cv_ptr;

    Mat imgLines;
    Mat imgThresholded;

    int iLowH;
    int iHighH;

    int iLowS;
    int iHighS;

    int iLowV;
    int iHighV;
    
    int posX;
    int posY;
    int imgRows;
    int imgCols;
    
    ros::Publisher marker_pub;
    visualization_msgs::Marker marker;
    float markerPulseIndex;
    
    ros::Publisher console_pub;
    
    int currentColor;

public:
    /**
     * Default constructor
     * This constructor initializes the variables, sets up the publisher and subscriber
     */
    Qual1()
        : it_(nh_)
    {
        
        posX = NO_BUTTON_FOUND;
        posY = NO_BUTTON_FOUND;
        
        // Subscribe to the camera's video feed
        image_sub_ = it_.subscribe("/multisense/camera/right/image_color", 1,
                                   &Qual1::imageCb, this);
        
        // Subscribe to the camera's point cloud data
        sub = nh_.subscribe ("/multisense/image_points2_color", 1,
                             &Qual1::cloud_cb, this);
        
        // Publisher to help visualize our answer
        marker_pub = nh_.advertise<visualization_msgs::Marker>("visualization_marker", 1);

        // Publish to submit our answers
        console_pub = nh_.advertise<srcsim::Console>("/srcsim/qual1/light", 1);
        
        // Create a new window that will show the images from the robot's camera
        cv::namedWindow(OPENCV_WINDOW, CV_WINDOW_AUTOSIZE);

        // Create a new window that will hold our threshold image
        cv::namedWindow(THRESHOLD_WINDOW, CV_WINDOW_AUTOSIZE);

        // Load the default image
        defaultImage_ = cv::imread( "/home/rhubarb/catkin_ws/src/src_qual1/media/rhubarb.jpg", 1 );

        // Set the variable saying that we should show the default image until we receive images from the robot
        shouldShowDefaultImg_ = 1;

        // The threshold variables
        searchForRed();

        // Double check that our default image loaded correctly
        if( !defaultImage_.data )
        {
            ROS_INFO_ONCE( "No image data" );
        }
        else
        {
            // If it did load correctly...

            // Make the imgLines image all black
            imgLines = Mat::zeros( defaultImage_.size(), CV_8UC3 );
        }
        
        // Set initial button location
        buttonLocation.x = 0;
        buttonLocation.y = 0;
        buttonLocation.z = -5000;        
        
        // Set the frame ID and timestamp.  See the TF tutorials for information on these.
        marker.header.frame_id = "/head";
        // Set the namespace and id for this marker.  This serves to create a unique ID
        // Any marker sent with the same namespace and id will overwrite the old one
        marker.ns = "qual1";
        marker.id = 0;
        marker.type = visualization_msgs::Marker::SPHERE;
        marker.action = visualization_msgs::Marker::ADD;
        marker.lifetime = ros::Duration();
        markerPulseIndex = 0;
    }

    /**
     * The deconstructor
     */
    ~Qual1()
    {
        cv::destroyWindow(OPENCV_WINDOW);
        cv::destroyWindow(THRESHOLD_WINDOW);
    }

    /**
     * The callback function for when we receive pointcloud data from the robot
     */
    void cloud_cb (const sensor_msgs::PointCloud2ConstPtr& input)
    {
        if( (posX != NO_BUTTON_FOUND) && (posY != NO_BUTTON_FOUND) )
        {
            // Create a container for the data.
            sensor_msgs::PointCloud2 output;
            
            pcl_ros::transformPointCloud("/head", *input, output, listener);
            
            // Transform from camera to head coordinate space
            
            // Convert the sensor_msgs/PointCloud2 data to pcl/PointCloud
            pcl::PointCloud<pcl::PointXYZRGB> cloud;
            pcl::PointCloud<pcl::PointXYZRGB> cloudTranformed;
            pcl::fromROSMsg (*input, cloud);

            tf::StampedTransform transform;
            try{
                // TODO: Use timestamp from message
                //right_camera_optical_frame
                listener.lookupTransform("/multisense/head_root", "/right_camera_optical_frame", 
                                         ros::Time(0), transform);
            }
            catch (tf::TransformException ex){
                ROS_ERROR("%s",ex.what());
            }
            
            pcl_ros::transformPointCloud(cloud, cloudTranformed, transform);            
            
            pcl::toROSMsg (cloudTranformed, output);
            
            // Find the 3D point based on where we found the button in our 2D image
            pixelTo3DPoint(output);
            
            // TODO: Send results once?
            sendResults();
        }
        
        publishMarker();
    }
    
    /**
     * The callback function for when we receive image data from the robot
     */
    void imageCb(const sensor_msgs::ImageConstPtr& msg)
    {
        // Make a variable to keep track of if we have printed the image dimensions
        static bool shouldPrintImgSize = true;

        // Convert the image into something that OpenCV can use
        try
        {
            cv_bridge::CvImagePtr cvImageBeforeRotationPtr;
            
            // Convert the image into both pointers because it sets both variables up for us
            cvImageBeforeRotationPtr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
            cv_ptr                   = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
            //cv::flip( cvImageBeforeRotationPtr->image, cv_ptr->image, -1); // -1 = Flip horizontally and vertically
            
            // Print the size of our image (number of rows and columns), but only print it once
            if(shouldPrintImgSize)
            {
                imgRows = (cv_ptr->image).rows;
                imgCols = (cv_ptr->image).cols;
                
                ROS_INFO("The images from the multisense camera are %d pixels wide and %d pixels tall", imgCols, imgRows );
                shouldPrintImgSize = false;
            }
        }
        catch (cv_bridge::Exception& e)
        {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return;
        }

        // We should no longer show the default image
        shouldShowDefaultImg_ = 0;

        // Find the red, green, or blue light
        performSearchForColors();
    }
    
    /**
     * Send the location, in meters relative to the head frame of R3,
     * and color, using a range of (0,1) for each RGB component, of each LED
     * to the /srcsim/qual1/light topic.
     */
    void sendResults()
    {
        srcsim::Console consoleMsg;
        
        consoleMsg.x = buttonLocation.x;
        consoleMsg.y = buttonLocation.y;
        consoleMsg.z = buttonLocation.z;

        consoleMsg.r = 0;
        consoleMsg.g = 0;
        consoleMsg.b = 0;
        
        if(currentColor == CURRENT_COLOR_RED)
        {
            consoleMsg.r = 1;
        }
        else if(currentColor == CURRENT_COLOR_GREEN)
        {
            consoleMsg.g = 1;
        }
        else if(currentColor == CURRENT_COLOR_BLUE)
        {
            consoleMsg.b = 1;
        }
        else
        {
            ROS_WARN("current color not set");
        }
        
        console_pub.publish(consoleMsg);
    }
    
    /**
     * Function to convert 2D pixel point to 3D point by extracting point
     * from PointCloud2 corresponding to input pixel coordinate. This function
     * can be used to get the X,Y,Z coordinates of a feature using an 
     * RGBD camera, e.g., Kinect.
     * 
     * This function, in part, utilizes code found at:
     *    http://answers.ros.org/question/191265/pointcloud2-access-data/
     */
    void pixelTo3DPoint(const sensor_msgs::PointCloud2 pointClound)
    {
        // Get the point corresponding to the center of the button
        int pointIndex = (posY * pointClound.row_step) + (posX * pointClound.point_step);
        
        // compute position in array where x,y,z data start
        int arrayPosX = pointIndex + pointClound.fields[0].offset; // X has an offset of 0
        int arrayPosY = pointIndex + pointClound.fields[1].offset; // Y has an offset of 4
        int arrayPosZ = pointIndex + pointClound.fields[2].offset; // Z has an offset of 8
        
        float X = 0.0;
        float Y = 0.0;
        float Z = 0.0;
        
        memcpy(&X, &pointClound.data[arrayPosX], sizeof(float));
        memcpy(&Y, &pointClound.data[arrayPosY], sizeof(float));
        memcpy(&Z, &pointClound.data[arrayPosZ], sizeof(float));
        
        ROS_INFO("Point %d at (%f, %f, %f)", pointIndex, X, Y, Z);
        
        buttonLocation.x = X;
        buttonLocation.y = Y;
        buttonLocation.z = Z;
    }
    
    void publishMarker()
    {
        marker.header.stamp = ros::Time::now();
        
        // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
        if(buttonLocation.x != buttonLocation.x)
        { // Check for NAN
            marker.pose.position.x = 0;
            marker.pose.position.y = 0;
            marker.pose.position.z = -5000;
        }
        else
        {
            marker.pose.position.x = buttonLocation.x;
            marker.pose.position.y = buttonLocation.y;
            marker.pose.position.z = buttonLocation.z;
            
            // Set the scale of the marker -- 1x1x1 here means 1m on a side
            markerPulseIndex += 0.2;
            marker.scale.x = 0.01+(sin(markerPulseIndex)/10);
            marker.scale.y = 0.01+(sin(markerPulseIndex)/10);
            marker.scale.z = 0.01+(sin(markerPulseIndex)/10);
        }
        
        marker.pose.orientation.x = 0.0;
        marker.pose.orientation.y = 0.0;
        marker.pose.orientation.z = 0.0;
        marker.pose.orientation.w = 1.0;
        
        // Set the color -- be sure to set alpha to something non-zero!
        marker.color.r = 1.0f;
        marker.color.g = 1.0f;
        marker.color.b = 0.0f;
        marker.color.a = 1.0;
        
        marker_pub.publish(marker);
    }

    /**
     * Display the default image in the main image window
     */
    void showDefaultImg()
    {
        // Double check to see if we properly loaded our default image
        if( !defaultImage_.data )
        {
            ROS_WARN_ONCE( "No image data" );
        }
        else
        {
            // If we propoerly loaded our default image, show it on the screen
            cv::imshow(OPENCV_WINDOW, defaultImage_ );
        }

        // Wait a little bit so the computer has time to render the image
        cv::waitKey(5); // 5ms
    }

    /**
     * Returns true if we should show the default image. Otherwise returns false.
     */
    bool shouldShowDefaultImg()
    {
        return shouldShowDefaultImg_;
    }
    
    void performSearchForColors()
    {
        searchForRed();
        if( !findColorBlob() )
        {
            searchForGreen();
            if( !findColorBlob() )
            {
                searchForBlue();
                findColorBlob();
            }
        }
    }
    
    /**
     * This function makes sure your HSV values are within the proper range.
     * It assumes you have fixed hue values to be half their proper value already.
     */
    void fixHSVOutOfBounds()
    {
        if(iLowH < 0)
        {
            iLowH = 0;
        }
        if(iHighH > 179)
        {
            iHighH = 179;
        }
        if(iLowS < 0)
        {
            iLowS = 0;
        }
        if(iHighS > 255)
        {
            iHighS = 255;
        }
        if(iLowV < 0)
        {
            iLowV = 0;
        }
        if(iHighV > 255)
        {
            iHighV = 255;
        }
    }
    
    void searchForRed()
    {
        currentColor = CURRENT_COLOR_RED;
        
        iLowH  = RED_HUE - HSV_WINDOW;  // low hue value
        iHighH = RED_HUE + HSV_WINDOW; // high hue value
        iLowS  = RED_SATURATION - HSV_WINDOW;  // low saturation value
        iHighS = RED_SATURATION + HSV_WINDOW; // high saturation value
        iLowV  = RED_VALUE - HSV_WINDOW;  // low value... value
        iHighV = RED_VALUE + HSV_WINDOW; // high value... value
        
        fixHSVOutOfBounds();
    }

    void searchForGreen()
    {
        currentColor = CURRENT_COLOR_GREEN;
        
        iLowH  = GREEN_HUE - HSV_WINDOW;  // low hue value
        iHighH = GREEN_HUE + HSV_WINDOW; // high hue value
        iLowS  = GREEN_SATURATION - HSV_WINDOW;  // low saturation value
        iHighS = GREEN_SATURATION + HSV_WINDOW; // high saturation value
        iLowV  = GREEN_VALUE - HSV_WINDOW;  // low value... value
        iHighV = GREEN_VALUE + HSV_WINDOW; // high value... value
        
        fixHSVOutOfBounds();
    }
    
    void searchForBlue()
    {
        currentColor = CURRENT_COLOR_BLUE;
        
        iLowH  = BLUE_HUE - HSV_WINDOW;  // low hue value
        iHighH = BLUE_HUE + HSV_WINDOW; // high hue value
        iLowS  = BLUE_SATURATION - HSV_WINDOW;  // low saturation value
        iHighS = BLUE_SATURATION + HSV_WINDOW; // high saturation value
        iLowV  = BLUE_VALUE - HSV_WINDOW;  // low value... value
        iHighV = BLUE_VALUE + HSV_WINDOW; // high value... value
        
        fixHSVOutOfBounds();
    }
    
    /**
     * Find the color blob based on the setting in the control window
     */
    bool findColorBlob()
    {
        bool returnValue = false;
        Mat imgHSV; // We'll need this for doing calculations

        // Double check to see if we have an actual image from the robot
        if(cv_ptr->image.data)
        {
            imgLines = Mat::zeros( cv_ptr->image.size(), CV_8UC3 ); // Fill the imgLines image with all black
            cvtColor(cv_ptr->image, imgHSV, COLOR_BGR2HSV); //Convert the image from BGR to HSV
        }
        else if( defaultImage_.data )
        {
            // If we don't have an image from the robot, check to see that the default image loaded correctly

            imgLines = Mat::zeros( defaultImage_.size(), CV_8UC3 ); // Fill the imgLines image with all black
            cvtColor(defaultImage_, imgHSV, COLOR_BGR2HSV); //Convert the image from BGR to HSV
        }
        else
        {
            ROS_WARN_ONCE( "No image data" );
        }

        inRange(imgHSV, Scalar(iLowH, iLowS, iLowV), Scalar(iHighH, iHighS, iHighV), imgThresholded); //Threshold the image

        //morphological opening (removes small objects from the foreground)
        erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );
        dilate( imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );

        //morphological closing (removes small holes from the foreground)
        dilate( imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );
        erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );

        //Calculate the moments of the thresholded image
        Moments oMoments = moments(imgThresholded);

        double dM01 = oMoments.m01;
        double dM10 = oMoments.m10;
        double dArea = oMoments.m00;

        // if the area <= 10000, I consider that the there are no object in the image and it's because of the noise, the area is not zero
        if (dArea > 10000)
        {

            //calculate the position of the color blob
            posX = dM10 / dArea;
            posY = dM01 / dArea;

            line(imgLines, Point(posX - 10, posY),    Point(posX + 10, posY   ), Scalar(255, 255, 255), 2);
            line(imgLines, Point(posX,    posY - 10), Point(posX,    posY + 10), Scalar(255, 255, 255), 2);


            if(cv_ptr->image.data)
            {
                imgLines = cv_ptr->image + imgLines;
            }
            else if( defaultImage_.data )
            {
                imgLines = defaultImage_ + imgLines;
            }


            if( !imgThresholded.data )
            {
                ROS_WARN_ONCE( "No image data" );
            }
            else
            {
                imshow(THRESHOLD_WINDOW, imgThresholded); //show the thresholded image
            }
            cv::waitKey(25);

            if( !imgLines.data )
            {
                imshow(OPENCV_WINDOW, cv_ptr->image); //show the combined image
            }
            else
            {
                imshow(OPENCV_WINDOW, imgLines); //show the combined image
            }

            returnValue = true;
        }
        else
        {
            posX = NO_BUTTON_FOUND;
            posY = NO_BUTTON_FOUND;
            
            // Could not find the color blob
            imshow(OPENCV_WINDOW, cv_ptr->image); //show the combined image

            imgThresholded = Mat::zeros( cv_ptr->image.size(), CV_8UC3 ); // Fill the imgThresholded image with all black
            imshow(THRESHOLD_WINDOW, imgThresholded); //show the thresholded image
        }

        cv::waitKey(25); // ms
        return returnValue;
    }
}; // End of Qual1 class


/**
 * The main function - This is where the program starts
 */
int main(int argc, char** argv)
{
    // Initialize ROS stuff
    ros::init(argc, argv, "qual1");

    // Create our Qual1 object
    Qual1 myQual1;

    // Keep looping until we're done
    while( ros::ok() )
    {
        // Show the default image if we are supposed to show the default image
        if( myQual1.shouldShowDefaultImg() )
        {
            myQual1.showDefaultImg();
        }

        // Allow other ROS nodes to do their thing
        ros::spinOnce();
    }
    return 0;
}
