# README #
Space Robotics Challenge  
Team Rhubarb  
Qualification Task 1  

### Setup ###
* [Create a catkin workspace](http://wiki.ros.org/catkin/Tutorials/create_a_workspace)

### Compile ###

```
$ cd ~/catkin_ws/
$ catkin_make
```

### Run ###
```
$ source ~/catkin_ws/devel/setup.bash
$ roslaunch srcsim qual1.launch extra_gazebo_args:="-r" init:="true"
$ roslaunch src_qual1 src_qual1.launch
```